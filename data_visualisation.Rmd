---
title: "Data Visualisation"
author: "Marija Dimitrievska"
date: "07/06/2021"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Plotting the data

Plotting the conservation of driver mutation based on their driver status
```{r}
library(ggplot2)
library(RColorBrewer)

#PhyloP
plot1 <- intOGenDriverMut[c(9,23)]
colnames(plot1) <- c("DriverMutPredicition", "MutConsScore")

plot1$mean <- (ave(plot1$MutConsScore, 
                            as.factor(plot1$DriverMutPredicition), 
                            FUN=function(x) mean(x, na.rm=TRUE))) 
#get the mean cons score for each driver mut status - use to colour the boxplots accordingly

plot1$DriverMutPredicition <- factor(plot1$DriverMutPredicition,
                                     levels = c("TIER 1", "TIER 2", "passenger"),
                                     labels = c("Tier 1", "Tier 2", "Passengers"))
summary(plot1)
p1 <- ggplot(plot1, aes(x=DriverMutPredicition, y=MutConsScore, fill=mean,
                        group=DriverMutPredicition))+
  geom_boxplot(aes(x = DriverMutPredicition, y = MutConsScore),
                   outlier.color= alpha("gray", 0.3), lwd=0.3, fatten = 1.3)+
  scale_fill_gradientn(name = "Mean Score", limits = c(-10, 10), colours=rev(brewer.pal(n=11, name = "RdYlBu")))+
  scale_x_discrete("Driver mutation prediction") +
  scale_y_continuous("PhyloP conservation score") +
  ggtitle("Distribution of conservation scores for \n each driver mutation status") +
  theme(plot.title = element_text(size = 12, hjust = 0.5), axis.text = element_text(size = 8), axis.title = element_text(size = 10), legend.key.size = unit(x = 0.4, units = "cm"), legend.text = element_text(size=8) )
p1

```


Plotting the conservation scores of the flanking sequences
```{r,message=FALSE}
library(tidyr)
library(plyr)
library(dplyr)

# phyloP
plot3 <- intOGenDriverMut[c(9,13:33)]
colnames(plot3) <- c("DriverMutPredicition", -10:10)
plot3$DriverMutPredicition <- factor(plot3$DriverMutPredicition,
                                   levels = c("TIER 1", "TIER 2", "passenger"),
                                   labels = c("Tier 1", "Tier 2", "Passengers"))

plot3 <- plot3 %>% 
  gather(key = "Position", value = "Score", -DriverMutPredicition)
plot3$Position <- as.numeric(plot3$Position)

mean3 <- plot3 %>% 
  group_by(DriverMutPredicition, Position) %>% 
  summarize_at(vars(Score), funs(mean(., na.rm=TRUE)))
#mean values at each position for all 3 driver statuses

plot3 <- left_join(x = plot3, y = mean3, 
                   by = c("DriverMutPredicition"="DriverMutPredicition",
                                                "Position"="Position"))
colnames(plot3)[3:4] <- c("Score", "Mean")
plot3$Position <- as.character(plot3$Position)


ggplot(plot3, aes(x=Position, y=Score, fill=Mean, group=Position))+
  geom_boxplot(outlier.color= alpha("gray", 0.3), lwd=0.3, fatten = 1.3)+
  scale_fill_gradientn(limits = c(-10, 10), 
                       colours=rev(brewer.pal(n=11, name = "RdYlBu"))) +
  scale_x_discrete("Position", labels=c(-10:10)) +
  scale_y_continuous(breaks=(seq(-10, 10, 5)), limits = c(-10, 10), "PhyloP conservation score" ) +
  ggtitle("Conservation scores of the flanking sequence \n for each driver mutation status") +
  theme(plot.title = element_text(size = 12, hjust = 0.5), 
        axis.text = element_text(size = 7))+
  facet_grid(DriverMutPredicition ~.)

ggsave("flankCons.png", width = 16, height = 10, units = "cm")

```


Making a heatmap of the mean conservation scores of the context of a mutation 
at a specific position
```{r, message = FALSE}
library(pheatmap)

heatmap_data_PhyloP <- ddply(intOGenDriverMut, c("flank"), summarise, 
                         mean_score1=mean(`PhyloP_-1`, na.rm=TRUE), 
                         mean_score2=mean(PhyloP_0, na.rm=TRUE), 
                         mean_score3=mean(PhyloP_1, na.rm=TRUE))

rownames(heatmap_data_PhyloP) <- heatmap_data_PhyloP[,1]
heatmap_data_PhyloP <- as.matrix(heatmap_data_PhyloP[,-1])
colnames(heatmap_data_PhyloP) <- c(-1,0,1)

#pheatmap plots
pheatmap(heatmap_data_PhyloP, cluster_cols=FALSE, 
         fontsize_row = 5, angle_col = 0, 
         main= "Conservation of different mutational contexts")

```


Heatmaps of the conservation of the mutational contexts were generated again, this time using the reverse compliments of the contexts to simplify the data
```{r}
heatmap_data_PhyloP_RC <- ddply(intOGenDriverMut, c("reverse_compliments"), summarise,
                         mean_score1=mean(`PhyloP_-1`, na.rm=TRUE), 
                         mean_score2=mean(PhyloP_0, na.rm=TRUE), 
                         mean_score3=mean(PhyloP_1, na.rm=TRUE))
  
rownames(heatmap_data_PhyloP_RC) <- heatmap_data_PhyloP_RC[,1]
heatmap_data_PhyloP_RC <- as.matrix(heatmap_data_PhyloP_RC[,-1])
colnames(heatmap_data_PhyloP_RC) <- c(-1,0,1)

pheatmap(heatmap_data_PhyloP_RC, cluster_cols=FALSE, fontsize = 25,
         fontsize_row = 15, fontsize_col = 20,  angle_col = 0,
         main= "Conservation of the different mutational contexts",
         filename="contextConsHeatmap.png", width = 16, height = 12)
```

# Statistical analysis 


Bootstapping was performed to test if the difference in means is real when the sample size is equal
```{r}
# bootstrapping sampling of mean

meanOfVariable <- function(data, indices, variable) {
  d <- data[indices,] # allows boot to select sample
  if(nrow(d) > 0){
    return( mean(d[, variable], na.rm = TRUE) )
  } else return(NA)
}

# calculation of mean for each group of mutations
x1 <- "PhyloP_0" # select which column to consider
tb1 <- lapply(c("passenger", "TIER 1", "TIER 2"), function(r){
  b <- intOGenDriverMut[which(intOGenDriverMut$driver_mut_prediction == r), ]
  proportion_boot <- boot::boot(data=b, statistic=meanOfVariable, R=1000, variable = x1 )
  proportion_CI <- boot::boot.ci(proportion_boot, conf = 0.95, type = "perc")
  proportion <- proportion_CI$t0
  proportion_CI <- as.numeric(proportion_CI$percent[1, 4:5])
  out <- data.frame(driver_mut_prediction = r, proportion = proportion,
                    lowerci = proportion_CI[1], upperci = proportion_CI[2])
  return( out )
})
tb1 <- do.call("rbind", tb1)

```


Bootstrapping data combined with the conservation score plots
```{r}
#PhyloP
tb1$driver_mut_prediction <- factor(tb1$driver_mut_prediction,
                                     levels = c("TIER 1", "TIER 2", "passenger"),
                                     labels = c("Tier 1", "Tier 2", "Passengers"))

p2 <- ggplot(tb1, aes(driver_mut_prediction, proportion))+
  geom_point(size = 2, colour = "red") +
  geom_errorbar(aes(x = driver_mut_prediction, ymin = lowerci, ymax = upperci), width = 0.4, lwd = 0.3)+
  scale_x_discrete("Driver mutation prediction") +
  ylab("Mean PhyloP score") +
  ylim(-10, 10) +
  ggtitle("Estimation of sample means \n with a 95% confidence") +
  theme(plot.title = element_text(hjust = 0.5, size = 12), axis.text = element_text(size = 8), axis.title = element_text(size = 10))
p2
ggsave("DriverStats.png", width = 6, height= 8, units = "cm")

library(patchwork)
p1 + p2 #phyloP
ggsave("DriverGroupCons.png", width = 16, height= 9, units = "cm")
```


Pairwise meadian test - tests the CI, tells us what to expect
```{r}
pairwiseMedianTest <- function(input, category_column, categories, data_column,
                               sample_times = 1000, sample_size = 10000){
  actual1 <- input[which(input[, category_column] == categories[1]), data_column]
  actual2 <- input[which(input[, category_column] == categories[2]), data_column]
  actual_diff <- abs(median(actual1, na.rm = TRUE) - median(actual2, na.rm = TRUE))
  null_diff <- sapply(1:sample_times, function(x){
    n <- sample(input[which(input[, category_column] %in% categories), data_column], 
                sample_size * 2, replace = TRUE)
    sample1 <- n[1:sample_size]; sample2 <- n[(sample_size+1):(sample_size*2)]
    return( abs(median(sample1, na.rm = TRUE) - median(sample2, na.rm = TRUE)) )
  })
  p <- sum( actual_diff > null_diff ) / length(null_diff)
  if(p > 0.5) return(1 - p) else return(p)
}

pairwiseMedianTest(intOGenDriverMut, "driver_mut_prediction", c("TIER 1", "TIER 2"), "PhyloP_0") #0.004
pairwiseMedianTest(intOGenDriverMut, "driver_mut_prediction", c("TIER 1", "passenger"), "PhyloP_0")
pairwiseMedianTest(intOGenDriverMut, "driver_mut_prediction", c("TIER 2", "passenger"), "PhyloP_0")

#p-val is significant for all - as expected/ seen in the plots, distributions are different, dont overlap


```


## COSMIC SBS examples

Subsetting for particular substitutions
```{r}
# C>T
CT_mean_PhyloP <-intOGenDriverMut %>% 
  dplyr::filter(base_change == "C>T") %>% 
  ddply(c("reverse_compliments"), summarise, 
                         mean_score1=mean(`PhyloP_-1`, na.rm=TRUE), 
                         mean_score2=mean(PhyloP_0, na.rm=TRUE), 
                         mean_score3=mean(PhyloP_1, na.rm=TRUE))

rownames(CT_mean_PhyloP) <- CT_mean_PhyloP[,1]
CT_mean_PhyloP <- as.matrix(CT_mean_PhyloP[,-1])
colnames(CT_mean_PhyloP) <- c(-1,0,1)

# C>G
CG_mean_PhyloP <-intOGenDriverMut %>% 
  dplyr::filter(base_change == "C>G") %>% 
  ddply(c("reverse_compliments"), summarise, 
                         mean_score1=mean(`PhyloP_-1`, na.rm=TRUE), 
                         mean_score2=mean(PhyloP_0, na.rm=TRUE), 
                         mean_score3=mean(PhyloP_1, na.rm=TRUE))

rownames(CG_mean_PhyloP) <- CG_mean_PhyloP[,1]
CG_mean_PhyloP <- as.matrix(CG_mean_PhyloP[,-1])
colnames(CG_mean_PhyloP) <- c(-1,0,1)

# T>A
TA_mean_PhyloP <-intOGenDriverMut %>% 
  dplyr::filter(base_change == "T>A") %>% 
  ddply(c("reverse_compliments"), summarise, 
                         mean_score1=mean(`PhyloP_-1`, na.rm=TRUE), 
                         mean_score2=mean(PhyloP_0, na.rm=TRUE), 
                         mean_score3=mean(PhyloP_1, na.rm=TRUE))

rownames(TA_mean_PhyloP) <- TA_mean_PhyloP[,1]
TA_mean_PhyloP <- as.matrix(TA_mean_PhyloP[,-1])
colnames(TA_mean_PhyloP) <- c(-1,0,1)

# T>C
TC_mean_PhyloP <-intOGenDriverMut %>% 
  dplyr::filter(base_change == "T>C") %>% 
  ddply(c("reverse_compliments"), summarise, 
                         mean_score1=mean(`PhyloP_-1`, na.rm=TRUE), 
                         mean_score2=mean(PhyloP_0, na.rm=TRUE), 
                         mean_score3=mean(PhyloP_1, na.rm=TRUE))

rownames(TC_mean_PhyloP) <- TC_mean_PhyloP[,1]
TC_mean_PhyloP <- as.matrix(TC_mean_PhyloP[,-1])
colnames(TC_mean_PhyloP) <- c(-1,0,1)

breaks = seq(1, 5, length.out = 101)

```

Conservation of different Single Base Substitution (SBS) Signatures

SBS1 signature - Ageing signature
Heatmaps with only C>T change
```{r}
SBS1 <- CT_mean_PhyloP[substr(rownames(CT_mean_PhyloP), 2, 3) %in% "CG",]

pheatmap(SBS1, cluster_rows=FALSE, cluster_cols=FALSE, 
         fontsize = 22, angle_col = 0, breaks = breaks,
         main="Conservation of 3 base motifs in a \n 5'-CG context in C>T mutations",
         filename="SBS1Cons.png", width=14, height=8)
```


SBS2 and SBS13 - APOBEC 
```{r}
#APOBEC
SBS2 <- CT_mean_PhyloP[substr(rownames(CT_mean_PhyloP), 1, 2) %in% "TC",]
SBS13 <- CG_mean_PhyloP[substr(rownames(CG_mean_PhyloP), 1, 2) %in% "TC",]


pheatmap(SBS2, cluster_rows=FALSE, cluster_cols=FALSE, 
         fontsize = 22, angle_col = 0, breaks = breaks,
         main="Conservation of 3 base motifs in a \n 5'TC- context in C>T mutations",
         filename="SBS2Cons.png", width=14, height=8)

pheatmap(SBS13, cluster_rows=FALSE, cluster_cols=FALSE, 
         fontsize = 22, angle_col = 0, breaks = breaks,
         main="Conservation of 3 base motifs in a \n 5'TC- context in C>G mutations",
         filename="SBS13Cons.png", width=14, height=8)

```


SBS7 - UV Damage
```{r}
SBS7ab <- CT_mean_PhyloP[substr(rownames(CT_mean_PhyloP), 1, 2) %in% c("TC", "CC"),] #a an b 
SBS7c <- TA_mean_PhyloP[substr(rownames(TA_mean_PhyloP), 2, 3) %in% "TT",]
SBS7d <- TC_mean_PhyloP[substr(rownames(TC_mean_PhyloP), 2, 3) %in% "TT",]
SBS7cd <- rbind(SBS7c, SBS7d)

pheatmap(SBS7ab, cluster_rows=FALSE, cluster_cols=FALSE, 
         fontsize = 22, angle_col = 0, breaks = breaks,
         main="Conservation of 3 base motifs in a 5'CC- \n and 5'TC- context in C>T mutations",
         filename="SBS7abCons.png", width=14, height=8)

pheatmap(SBS7cd, cluster_rows=FALSE, cluster_cols=FALSE, 
         fontsize = 22, angle_col = 0, breaks = breaks,
         main="Conservation of 3 base motifs in a 5'-TT \n context in T>A and T>C mutations",
        filename="SBS7cdCons.png", width=14, height=8)

```


Plotting the frequency of position in the codon where mutations occur  
```{r}
CodonPos <- intOGenDriverMut[,c("mut_codon_pos", "driver_mut_prediction")]
CodonPos$mut_codon_pos <- as.factor(CodonPos$mut_codon_pos)
CodonPos$driver_mut_prediction <- factor(CodonPos$driver_mut_prediction,
                                   levels = c("TIER 1", "TIER 2", "passenger"),
                                   labels = c("Tier 1", "Tier 2", "Passengers"))

CodonPosT1 <- filter(CodonPos, driver_mut_prediction=="TIER 1")
CodonPosT2 <- filter(CodonPos, driver_mut_prediction=="TIER 2")
CodonPosP <- filter(CodonPos, driver_mut_prediction=="passenger")


ggplot(CodonPos, aes(x = driver_mut_prediction, fill = as.factor(mut_codon_pos), y = (..count..)/sum(..count..) ))+
  geom_bar(position = "fill", stat = "count")+
  xlab(label = NULL) +
  ylab(label = "Percentage") +
  scale_y_continuous(labels = percent) +
  labs(title = "Codon position of the mutated base", fill = "Codon position")+
  theme(plot.title = element_text(hjust = 0.5))+
  scale_fill_manual(values = c("#61BBB7", "#8EDBD9", "#FFA48E") )

```


PLotting the context frequency
```{r}
AA_change <- intOGenDriverMut[,c("reverse_compliments", "driver_mut_prediction", "base_change")]
AA_change$reverse_compliments <- as.factor(AA_change$reverse_compliments)


AA_changeSBS1 <- dplyr::filter(AA_change, base_change == "C>T")
AA_changeSBS1 <- AA_changeSBS1[substr((AA_changeSBS1$reverse_compliments), 2, 3) %in% "CG",]
AA_changeSBS1$driver_mut_prediction <- factor(AA_changeSBS1$driver_mut_prediction,
                                   levels = c("TIER 1", "TIER 2", "passenger"),
                                   labels = c("Tier 1", "Tier 2", "Passengers"))


AA_changeSBS2 <- dplyr::filter(AA_change, base_change == "C>T")
AA_changeSBS2 <- AA_changeSBS2[substr((AA_changeSBS2$reverse_compliments), 1, 2) %in% "TC",]
AA_changeSBS2$driver_mut_prediction <- factor(AA_changeSBS2$driver_mut_prediction,
                                   levels = c("TIER 1", "TIER 2", "passenger"),
                                   labels = c("Tier 1", "Tier 2", "Passengers"))

AA_changeSBS13 <- dplyr::filter(AA_change, base_change == "C>G")
AA_changeSBS13 <- AA_changeSBS13[substr((AA_changeSBS13$reverse_compliments), 1, 2) %in% "TC",]
AA_changeSBS13$driver_mut_prediction <- factor(AA_changeSBS13$driver_mut_prediction,
                                   levels = c("TIER 1", "TIER 2", "passenger"),
                                   labels = c("Tier 1", "Tier 2", "Passengers"))

AA_changeSBS7 <- dplyr::filter(AA_change, base_change == "C>T")
AA_changeSBS7$driver_mut_prediction <- factor(AA_changeSBS7$driver_mut_prediction,
                                   levels = c("TIER 1", "TIER 2", "passenger"),
                                   labels = c("Tier 1", "Tier 2", "Passengers"))
AA_changeSBS7a <- AA_changeSBS7[substr((AA_changeSBS7$reverse_compliments), 1, 2) %in% "TC",]
AA_changeSBS7b <- AA_changeSBS7[substr((AA_changeSBS7$reverse_compliments), 1, 2) %in% "CC",]
AA_changeSBS7a$sig <- "SBS7a"
AA_changeSBS7b$sig <- "SBS7b"

AA_changeSBS7c <- dplyr::filter(AA_change, base_change == "T>A")
AA_changeSBS7c <- AA_changeSBS7c[substr((AA_changeSBS7c$reverse_compliments), 2, 3) %in% "TT",]

AA_changeSBS7d <- dplyr::filter(AA_change, base_change == "T>C")
AA_changeSBS7d <- AA_changeSBS7d[substr((AA_changeSBS7d$reverse_compliments), 2, 3) %in% "TT",]
AA_changeSBS7c$sig <- "SBS7c"
AA_changeSBS7d$sig <- "SBS7d"


library(scales)
# SBS1 
ggplot(AA_changeSBS1, aes(x = driver_mut_prediction, fill = reverse_compliments ))+
  geom_bar(position = "fill", stat = "count")+
  xlab(label = NULL) +
  ylab(label = "Percentage") +
  scale_y_continuous(labels = percent) +
  labs(title = "Frequency of trinucleotide contexts with C>T mutations", fill = "Mutational context")+
  theme(plot.title = element_text(hjust = 0.5, size = 12),
        axis.text = element_text(size = 10))+
  scale_fill_manual(values = c( "#FFA48E", "#61BBB7", "#AADCFD", "#FDAAD9")) +
  geom_text(aes(label=..count..), stat="count", position=position_fill(vjust = 0.5))

ggsave("SBS1Freq.png", width = 16, height= 8, units = "cm")


# SBS2 and 13
ggplot(AA_changeSBS2, aes(x = driver_mut_prediction, fill = reverse_compliments ))+
  geom_bar(position = "fill", stat = "count")+
  xlab(label = NULL) +
  ylab(label = "Percentage") +
  scale_y_continuous(labels = percent) +
  labs(title = "Frequency of trinucleotide contexts with C>T mutations", fill = "Mutational context")+
  theme(plot.title = element_text(hjust = 0.5, size = 12),
        axis.text = element_text(size = 10))+
  scale_fill_manual(values = c( "#FFA48E", "#61BBB7", "#FDAAD9", "#AADCFD")) +
  geom_text(aes(label=..count..), stat="count", position=position_fill(vjust = 0.5))

ggsave("SBS2Freq.png", width = 16, height= 8, units = "cm")

ggplot(AA_changeSBS13, aes(x = driver_mut_prediction, fill = reverse_compliments ))+
  geom_bar(position = "fill", stat = "count")+
  xlab(label = NULL) +
  ylab(label = "Percentage") +
  scale_y_continuous(labels = percent) +
  labs(title = "Frequency of trinucleotide contexts with C>G mutations", fill = "Mutational context")+
  theme(plot.title = element_text(hjust = 0.5, size = 12),
        axis.text = element_text(size = 10))+
  scale_fill_manual(values = c( "#FFA48E", "#61BBB7", "#FDAAD9", "#AADCFD")) +
  geom_text(aes(label=..count..), stat="count", position=position_fill(vjust = 0.5))

ggsave("SBS13Freq.png", width = 16, height= 8, units = "cm")

# SBS7
AA_changeSBS7 <- rbind(AA_changeSBS7a, AA_changeSBS7b)
ggplot(AA_changeSBS7, aes(x = driver_mut_prediction, fill = reverse_compliments ))+
  geom_bar(position = "fill", stat = "count")+ facet_wrap(~sig)+
  xlab(label = NULL) +
  ylab(label = "Percentage") +
  scale_y_continuous(labels = percent) +
  labs(title = "Frequency of the trinucleotide contexts with C>T and C>G mutations", fill = "Mutational context")+
  theme(plot.title = element_text(hjust = 0.5, size = 12),
        axis.text = element_text(size = 10))+
  scale_fill_manual(values = c("#F57D7D", "#aeff89", "#da89ff", "#89e9ff", "#FFA48E", "#61BBB7", "#FDAAD9", "#AADCFD")) +
  geom_text(aes(label=..count..), stat="count", position=position_fill(vjust = 0.5))

ggsave("SBS7abFreq.png", width = 16, height= 8, units = "cm")

AA_changeSBS7cd <- rbind(AA_changeSBS7c, AA_changeSBS7d)
AA_changeSBS7cd$driver_mut_prediction <- factor(AA_changeSBS7cd$driver_mut_prediction,
                                   levels = c("TIER 1", "TIER 2", "passenger"),
                                   labels = c("Tier 1", "Tier 2", "Passengers"))
ggplot(AA_changeSBS7cd, aes(x = driver_mut_prediction, fill = reverse_compliments ))+
  geom_bar(position = "fill", stat = "count")+ facet_wrap(~sig)+
  xlab(label = NULL) +
  ylab(label = "Percentage") +
  scale_y_continuous(labels = percent) +
  labs(title = "Frequency of the trinucleotide contexts with T>A and T>C mutations", fill = "Mutational context")+
theme(plot.title = element_text(hjust = 0.5, size = 12),
        axis.text = element_text(size = 10))+
  scale_fill_manual(values = c("#fffea4", "#a4ffd1", "#ffa4d2", "#a4d2ff")) +
  geom_text(aes(label=..count..), stat="count", position=position_fill(vjust = 0.5))

ggsave("SBS7cdFreq.png", width = 16, height= 8, units = "cm")

```


Amino acid chenge plots
```{r}
type_change <- intOGenDriverMut %>% 
  dplyr::select("reverse_compliments", "driver_mut_prediction", "WT_AA", "MUT_AA", "wt_type", "mut_type", "change_type")

type_change$driver_mut_prediction <- factor(type_change$driver_mut_prediction,
                                   levels = c("TIER 1", "TIER 2", "passenger"),
                                   labels = c("Tier 1", "Tier 2", "Passengers"))

ggplot(type_change, aes(x = driver_mut_prediction, fill = as.factor(change_type)))+
  geom_bar(position = "fill", stat = "count")+
  xlab(label = NULL) +
  ylab(label = "Frequency") +
  labs(title = "Proportion of mutations resulting in amino acid type change", fill = "Physiochemical change")+
  theme(plot.title = element_text(hjust = 0.5))+
  scale_fill_manual(values = c("#FFA48E", "#61BBB7") )+
  scale_y_continuous(labels = percent) +
  geom_text(aes(label=..count..), stat="count", position=position_fill(vjust = 0.5))

#about 3/4  of mutations result in amino acid type change

# test which are the most common type changes
AAtype_change <- ddply(intOGenDriverMut, c("driver_mut_prediction", "wt_type", "mut_type"), nrow)
AAtype_change2 <- ddply(intOGenDriverMut, "driver_mut_prediction", nrow)
AAtype_change <- merge(AAtype_change, AAtype_change2, by = "driver_mut_prediction")
AAtype_change$percentage <- (AAtype_change$V1.x / AAtype_change$V1.y)*100
AAtype_change$driver_mut_prediction <- factor(AAtype_change$driver_mut_prediction,
                                   levels = c("TIER 1", "TIER 2", "passenger"),
                                   labels = c("Tier 1", "Tier 2", "Passengers"))
AAtype_change$wt_type <- factor(AAtype_change$wt_type,
                                   levels = c("aromatic", "negative", "positive", "polar", "nonpolar" ),
                                   labels = c("Aromatic", "Negative", "Positive", "Polar", "Nonpolar" ))
AAtype_change$mut_type <- factor(AAtype_change$mut_type,
                                   levels = c("aromatic", "negative", "positive", "polar", "nonpolar" ),
                                   labels = c("Aromatic", "Negative", "Positive", "Polar", "Nonpolar" ))
breaks1 = seq(0, 0.1, 0.2)
ggplot(AAtype_change, aes(x=wt_type, y=mut_type, fill=percentage)) +
  geom_tile() + facet_wrap(~driver_mut_prediction) +
  scale_fill_gradientn(name = "Percentage", limits = c(0, 20), 
                       colours=c("navyblue", "darkmagenta", "orange")) +
  theme(axis.text.x = element_text(angle = 45, size = 8)) + 
  xlab(label = "from Wildtype Amino Acid") +
  ylab(label = "to Mutated Amino Acid")
```


