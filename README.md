# A Bioinformatic Study of Cancer Driver Mutations and the Conservation of Mutational Signatures #

This repository contains all the R code used to analyse the data in this project.


### Content ###
`data_annotation`: R markdown workbook containing the pipeline used to annotate the somatic cancer mutation data. A tab-separated file with the mutations (e.g intOGen driver mutation catalogue) is required as input in the code.

`data_visualisation`: R markdown workbook containing the code used to generate the figures presented in the project.